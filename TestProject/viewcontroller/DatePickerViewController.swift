import UIKit

class DatePickerViewController: UIViewController {

    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var displayDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.datePicker.isHidden = true

        if let date = UserDefaults.standard.value(forKey: "lastInputDate") as? Date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
            dateFormatter.locale = Locale.current
            self.displayDate.text = dateFormatter.string(from: date)
            self.datePicker.setDate(date, animated: false)
        } else {
            self.displayDate.text = "沒輸入過日期"
        }
    }
    
    @IBAction func onDateSelected(_ sender: UIDatePicker) {
        let date = sender.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        dateFormatter.locale = Locale.current
        self.displayDate.text = dateFormatter.string(from: date)
        
        UserDefaults.standard.set(date, forKey: "lastInputDate")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func onShowDatePickerClicked(_ sender: Any) {
        self.datePicker.isHidden = false
    }
    
    
}
