//
//  GifTestViewController.swift
//  TestProject
//
//  Created by yomi on 2020/2/4.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class GifTestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let imageView = UIImageView(frame: CGRect(x: 30, y: 30, width: 201, height: 131))
        self.view.addSubview(imageView)
        
        let animatedImage = UIImage.animatedImageNamed("ec2763f11cb3409eec43bb5f558c0f93-", duration: 1)
        imageView.image = animatedImage
        imageView.stopAnimating()
    }
}
