import UIKit

class Test2ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        print("Hello App")
        let timeStr = "2/21/2020 10:18:40 AM"
        print("timeStr = \(timeStr)")
        var dateFormat:DateFormatter = DateFormatter()
        dateFormat.dateFormat = "MM/dd/yyyy HH:mm:ss a"
        dateFormat.locale = Locale.current
        dateFormat.amSymbol = "AM"
        dateFormat.pmSymbol = "PM"
        let date = dateFormat.date(from: timeStr)
        print("date is \(date)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let origin = CGPoint(x: 80, y: 80)
        let size = CGSize(width: 120, height: 120)
        let newView = UIView.init(frame: CGRect(origin: origin, size: size))
        newView.backgroundColor = .brown
        self.view.addSubview(newView)
        
        let origin2 = CGPoint(x: 120, y: 120)
        let size2 = CGSize(width: 80, height: 80)
        let newView2 = UIView.init(frame: CGRect(origin: origin2, size: size2))
        newView2.backgroundColor = .black
        self.view.addSubview(newView2)
    }
    
    
    @IBAction func onTap(_ sender: Any) {
        print("onTap")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
