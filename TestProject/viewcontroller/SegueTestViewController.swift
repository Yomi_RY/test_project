import UIKit
import AVKit

class SegueTestViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let timeStr = "2/21/2020 10:18:40 AM"
        print("timeStr = \(timeStr)")
        var dateFormat:DateFormatter = DateFormatter()
        dateFormat.dateFormat = "MM/dd/yyyy hh:mm:ss a"
        // dateFormat.dateFormat = "yyyyMMddHHmmss"
        let date = dateFormat.date(from: timeStr)
        print("date is \(date)")
        
//        if let url = URL(string: "https://fcit.usf.edu/matrix/wp-content/uploads/2017/11/sample.m4v?_=1") {
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                let player = AVPlayer(url: url)
//                let controller = AVPlayerViewController()
//                controller.player = player
//                
//                self.present(controller, animated: true) {
//                    player.play()
//                }
//            }
//        }
    }
    
}
