import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet var numBtns: [UIButton]!
    
    var mPrev:Int = 0
    var isAddClick:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func onNumClick(_ sender: UIButton) {
        let tag = sender.tag
        let curNum = self.resultLabel.text
        
        print("Num \(tag) pressed")
        if curNum == "0" {
            self.resultLabel.text = "\(tag)"
        } else {
            self.resultLabel.text?.append("\(tag)")
        }
    }
    
    @IBAction func onClearClicked(_ sender: Any) {
        self.resultLabel.text = "0"
//        self.mPrevResult = 0
    }
    
    
    @IBAction func addOperation(_ sender: Any) {
//        self.sum = self.sum + (Int(self.resultLabel.text ?? "0") ?? 0)
//        self.resultLabel.text = "\(self.sum)"
    }
    
    @IBAction func onEqualClicked(_ sender: Any) {
    }
    
}
