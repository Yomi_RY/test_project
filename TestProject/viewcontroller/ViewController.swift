

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        contentView.layer.shadowOpacity = 0.8
//        contentView.layer.cornerRadius = 100
//        imageView.clipsToBounds = true
//        imageView.layer.cornerRadius = 100
        let smallImage = resizeImage(image: UIImage(named: "Test-Photo1.jpeg")!, width: 200)
        imageView.image = smallImage
    }
    
    func resizeImage(image: UIImage, width: CGFloat) -> UIImage {
            let size = CGSize(width: width, height:
                image.size.height * width / image.size.width)
            let renderer = UIGraphicsImageRenderer(size: size)
            let newImage = renderer.image { (context) in
                image.draw(in: renderer.format.bounds)
            }
            return newImage
    }
    
}

