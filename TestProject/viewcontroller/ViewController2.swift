//
//  ViewController2.swift
//  TestProject
//
//  Created by yomi on 2020/2/5.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onBtnClicked(_ sender: Any) {
        self.textField.resignFirstResponder()
//        self.view.endEditing(true)
    }
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
