import UIKit

class AnimTest1ViewController: UIViewController {
    
    
    @IBOutlet weak var diceImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 3, delay: 0, animations: {
            self.diceImageView.center = CGPoint(x: self.diceImageView.center.x, y:self.diceImageView.frame.origin.y + 200)
        })
    }
}
