//
//  MapTestViewController.swift
//  TestProject
//
//  Created by yomi on 2019/12/25.
//  Copyright © 2019 yomi. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapTestViewController: UIViewController {
    
    private static let imageCacheKey: NSString = "CachedMapSnapshot"
    
    @IBOutlet weak var mMapLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mIvMapImage: UIImageView!
    
    var imageCache = NSCache<NSString, UIImage>()
    
    private func cacheImage(img:UIImage) {
        imageCache.setObject(img, forKey: MapTestViewController.imageCacheKey)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        mMapLoadingIndicator.isHidden = false
        mMapLoadingIndicator.startAnimating()
        
        let coords = CLLocationCoordinate2D(latitude: 52.239647, longitude: 21.045845)
        let distanceInMeters: Double = 500
        let options = MKMapSnapshotter.Options()
        options.region = MKCoordinateRegion(center: coords, latitudinalMeters: distanceInMeters, longitudinalMeters: distanceInMeters)
        options.size = mIvMapImage.frame.size
        
        let bgQueue = DispatchQueue.global(qos: .background)
        let snapShotter = MKMapSnapshotter(options: options)
        
        snapShotter.start(with: bgQueue) {[weak self] (snapshot, error) in
            guard error == nil else {
                return
            }
            
            if let snapShotImage = snapshot?.image, let coordinatePoint = snapshot?.point(for: coords), let pinImage = UIImage(named: "pin_notavailable") {
                
                UIGraphicsBeginImageContextWithOptions(snapShotImage.size, true, snapShotImage.scale)
                snapShotImage.draw(at: CGPoint.zero)
                /// 5.
                // need to fix the point position to match the anchor point of pin which is in middle bottom of the frame
                let fixedPinPoint = CGPoint(x: coordinatePoint.x - pinImage.size.width / 2, y: coordinatePoint.y - pinImage.size.height)
                pinImage.draw(at: fixedPinPoint)
                
                let amount:NSString = "60"
                let textColor = UIColor.black
                let textFont = UIFont(name: "Helvetica Bold", size: 12)!
                let textH = textFont.lineHeight
                let rect = CGPoint(x: coordinatePoint.x - (textH + pinImage.size.width) / 6 , y: coordinatePoint.y - pinImage.size.height * 3 / 4)
                // Setup the font attributes that will be later used to dictate how the text should be drawn
                let textFontAttributes = [
                    NSAttributedString.Key.font: textFont,
                    NSAttributedString.Key.foregroundColor: textColor,
                ]
                amount.draw(at: rect, withAttributes: textFontAttributes)
                
                let mapImage = UIGraphicsGetImageFromCurrentImageContext()
                if let unwrappedImage = mapImage {
                    self?.cacheImage(img: unwrappedImage)
                }
                
                /// 6.
                DispatchQueue.main.async {
                    self?.mIvMapImage.image = mapImage
                    self?.mMapLoadingIndicator.stopAnimating()
                    self?.mMapLoadingIndicator.isHidden = true
                }
                UIGraphicsEndImageContext()
            }
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
