//
//  ThirdViewController.swift
//  TestProject
//
//  Created by yomi on 2020/1/28.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let swipeGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(showFirstViewController))
        swipeGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.up
           self.view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    @objc func showFirstViewController() {
        self.performSegue(withIdentifier: "idSecondSegueUnwind", sender: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
