//
//  SecondViewController.swift
//  TestProject
//
//  Created by yomi on 2020/1/28.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    
    var message: NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("Message from FirstViewController is \(self.message)")
        var swipeGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "showFirstViewController")
        swipeGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    @objc func showFirstViewController() {
        self.performSegue(withIdentifier: "idFirstSegueUnwind", sender: self)
    }
}
