import UIKit

class FirstViewController: UIViewController {
    
    
    @IBOutlet var swipGesture: UISwipeGestureRecognizer!
    
    var message:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipGesture.direction = .up
        
        //        var swipeGestureRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(showSecondViewController))
        //        swipeGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.up
        //        self.view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    @IBAction func showSecondViewController(_ sender: Any) {
        self.performSegue(withIdentifier: "idFirstSegue", sender: self)
    }
    
    @IBAction func showThirdViewController(_ sender: Any) {
        self.performSegue(withIdentifier: "idSecondSegue", sender: self)
    }
    //    @objc func showSecondViewController() {
    //        self.performSegue(withIdentifier: "idFirstSegue", sender: self)
    //    }
    
    @IBAction func returnFromSegueActions(sender: UIStoryboardSegue){
        if sender.identifier == "idFirstSegueUnwind" {
            let originalColor = self.view.backgroundColor
            self.view.backgroundColor = UIColor.green
            
            UIView.animate(withDuration: 20.0, animations: { () -> Void in
                self.view.backgroundColor = originalColor
            })
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "idFirstSegue" {
            let secondViewController = segue.destination as! SecondViewController
            secondViewController.message = "Hello from the 1st View Controller"
        }
    }
    
    override func segueForUnwinding(to toViewController: UIViewController, from fromViewController: UIViewController, identifier: String?) -> UIStoryboardSegue {
        if let id = identifier{
            if id == "idFirstSegueUnwind" {
                let unwindSegue = FirstCustomSegueUnwind(identifier: id, source: fromViewController, destination: toViewController, performHandler: { () -> Void in
                    
                })
                return unwindSegue
            } else if id == "idSecondSegueUnwind" {
                let unwindSegue = SecondCustomSegueUnwind(identifier: id,
                                                          source: fromViewController,
                                                          destination: toViewController,
                                                          performHandler: { () -> Void in
                                                            
                })
                
                return unwindSegue
            }
        }
        
        return super.segueForUnwinding(to: toViewController, from: fromViewController, identifier: identifier)!
    }
}
