//
//  FirstCustomSegueUnwind.swift
//  TestProject
//
//  Created by yomi on 2020/1/28.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class FirstCustomSegueUnwind: UIStoryboardSegue {
    
    override func perform() {
        // 指定來源與目標視圖給區域變數
        var secondVCView = self.source.view as UIView
        var firstVCView = self.destination.view as UIView
        let screenHeight = UIScreen.main.bounds.size.height
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(firstVCView, aboveSubview: secondVCView)
        
        // Animate the transition.
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            firstVCView.frame = firstVCView.frame.offsetBy(dx: 0.0, dy: screenHeight)
            secondVCView.frame = secondVCView.frame.offsetBy(dx: 0.0, dy: screenHeight)
            
        }) { (Finished) -> Void in
            
            self.source.dismiss(animated: false, completion: nil)
        }
    }
}
