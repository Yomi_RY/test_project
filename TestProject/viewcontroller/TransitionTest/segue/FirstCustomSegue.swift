//
//  FirstCustomSegue.swift
//  TestProject
//
//  Created by yomi on 2020/1/28.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class FirstCustomSegue: UIStoryboardSegue {

    
    override func perform() {
        // 指定來源與目標視圖給區域變數
        var firstVCView = self.source.view as UIView
        var secondVCView = self.destination.view as UIView
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        // 指定目標視圖的初始位置
        secondVCView.frame = CGRect(x: 0.0, y: screenHeight, width: screenWidth, height: screenHeight)
        // 取得App的 key window 並插入目標視圖至目前視圖（來源視圖）上
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(secondVCView, aboveSubview: firstVCView)
        UIView.animate(withDuration: 0.4, animations: {
            firstVCView.frame = firstVCView.frame.offsetBy(dx: 0.0, dy: -screenHeight)
            secondVCView.frame = secondVCView.frame.offsetBy(dx: 0.0, dy: -screenHeight)
        }) { finish in
            self.source.present(self.destination as UIViewController,
            animated: false,
            completion: nil)
        }
    }
}
