//
//  SecondCustomSegue.swift
//  TestProject
//
//  Created by yomi on 2020/1/28.
//  Copyright © 2020 yomi. All rights reserved.
//

import UIKit

class SecondCustomSegue: UIStoryboardSegue {
    
    override func perform() {
        var firstVCView = self.source.view as UIView
        var thirdVCView = self.destination.view as UIView
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(thirdVCView, belowSubview: firstVCView)
        
        thirdVCView.transform = thirdVCView.transform.scaledBy(x: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            firstVCView.transform = thirdVCView.transform.scaledBy(x: 0.001, y: 0.001)
            
        }) { (Finished) -> Void in
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                thirdVCView.transform = CGAffineTransform.identity
                
            }, completion: { (Finished) -> Void in
                firstVCView.transform = CGAffineTransform.identity
                self.source.present(self.destination as UIViewController, animated: false, completion: nil)
            })
        }
    }
}
