//
//  VersionCheckViewController.swift
//  TestProject
//
//  Created by yomi on 2019/12/16.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation
import UIKit

class VersionCheckViewController: ViewController {
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        print("\(#function)")
    }
    
    override func viewDidLayoutSubviews() {
        print("\(#function)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("\(#function)")
    }
}
