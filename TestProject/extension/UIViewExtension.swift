import Foundation
import UIKit

extension UIView {
    @IBInspectable var cornerRadius:CGFloat {
         get {
             self.layer.cornerRadius
         }
         set {
             self.layer.cornerRadius = newValue
         }
     }
    
    @IBInspectable var borderColor:UIColor {
        get {
            UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        get {
            self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
}
