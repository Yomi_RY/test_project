//
//  TestClz.h
//  TestProject
//
//  Created by yomi on 2020/1/5.
//  Copyright © 2020 yomi. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestClz : NSObject

+(void) printTestClz;
@end

NS_ASSUME_NONNULL_END
