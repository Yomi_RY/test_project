//
//  ApiUtils.swift
//  InAppSDKDemo
//
//  Created by yomi on 2019/11/20.
//  Copyright © 2019 CyberSource, a Visa Company. All rights reserved.
//

import Foundation
import Alamofire


class ApiUtils {
    public static func checkVersionUpdate(data:AppVersionInfo
        , onSuccess:@escaping(VersionCheckResponse) -> Void
        , onFailure:@escaping(NSError) -> Void) -> Void {
        let urlStr = Constants.CMS_DEV_BASE_URL + Constants.CMS_API_CHECK_VERSION
        let params = data.getBodyParams()
        
        print("\(#function)")
        print("params \n\n\(params)")
        print(">>> api")
        AF.request(urlStr, method: .post, parameters:params).responseDecodable { (response:DataResponse<VersionCheckResponse, AFError>) in
            let error = response.error
            
            guard let versionCheckResp = response.value, error == nil else {
                var errorMsg:String
                
                if(response.value == nil) {
                    errorMsg = "Response is nil"
                } else {
                    errorMsg = "Error is \(error?.errorDescription ?? "")"
                }
                
                let error = NSError(domain: errorMsg, code: -1, userInfo: nil)
                onFailure(error)
                return
            }
        
            onSuccess(versionCheckResp)
            print("<<< api")
        }
    }
}
