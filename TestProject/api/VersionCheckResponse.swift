//
//  VersionCheckResponse.swift
//  TestProject
//
//  Created by yomi on 2019/12/16.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation

class VersionCheckResponse:Codable {
    var NeedUpgrade:Bool?
    var Version:String?
    var AlwaysUpgrade:Bool?
    var AppUrl:String?
    var PublishDateTime: String?
}
