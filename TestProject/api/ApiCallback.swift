//
//  ApiCallback.swift
//  InAppSDKDemo
//
//  Created by yomi on 2019/11/20.
//  Copyright © 2019 CyberSource, a Visa Company. All rights reserved.
//

import Foundation


protocol ApiCallback {
    func onError(apiTag:String, data:AnyObject?)
    func onSuccess(apiTag:String, data:AnyObject?)
}
