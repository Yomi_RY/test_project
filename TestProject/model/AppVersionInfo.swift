//
//  AppVersionInfo.swift
//  TestProject
//
//  Created by yomi on 2019/12/16.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation

class AppVersionInfo: Codable{
    var HqId: String?
    var Sign: String?
    var Version: String?
    var BundleId: String?
    var AppType: String?
    
    func getBodyParams() -> [String:Any] {
        return [
            "HqId" : self.HqId!,
            "Sign" : self.Sign!,
            "Version": self.Version!,
            "BundleId": self.BundleId!,
            "AppType": self.AppType!
        ]
    }
}
