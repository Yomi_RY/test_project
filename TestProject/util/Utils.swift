//
//  Utils.swift
//  TestProject
//
//  Created by yomi on 2019/12/16.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation
import CommonCrypto

class Utils {
    static func convertToSha512(input: String) -> String {
        let data = input.data(using: .utf8) ?? Data()
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA512($0.baseAddress, CC_LONG(data.count), &digest)
        }
        return digest.map({ String(format: "%02hhx", $0) }).joined(separator: "")
    }
}
