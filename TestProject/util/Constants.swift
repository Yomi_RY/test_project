//
//  Constants.swift
//  TestProject
//
//  Created by yomi on 2019/12/16.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation

class Constants:Codable {
    public static let CMS_DEV_BASE_URL = "http://t-content-dev.azurewebsites.net"
    public static let CMS_API_CHECK_VERSION = "/api/EndPoint/v1.0/CheckAppVersion"
    public static let CMS_HQ_ID = "9957554366492BB4"
    public static var CMS_SIGN:String {
        get {
            return
                Utils.convertToSha512(input:("810d0d92-6799-4ab8-9b63-fea8c38c98c6".uppercased()))
        }
    }
    
}
