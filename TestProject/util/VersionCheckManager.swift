//
//  VersionCheckManager.swift
//  TestProject
//
//  Created by yomi on 2019/12/17.
//  Copyright © 2019 yomi. All rights reserved.
//

import Foundation
import UIKit

class VersionCheckManager {
    
    private static var sInstance:VersionCheckManager? = nil
    
    static func getInstance() -> VersionCheckManager? {
        if(sInstance == nil) {
            sInstance = VersionCheckManager()
        }
        return sInstance
    }
    
    func checkVersion(controller:UIViewController?) {
        guard let vc = controller else {
            return
        }
        
        let appVersionInfo = AppVersionInfo()
        appVersionInfo.HqId = Constants.CMS_HQ_ID
        appVersionInfo.Sign = Constants.CMS_SIGN
        appVersionInfo.AppType = "IOS"
        appVersionInfo.BundleId = Bundle.main.bundleIdentifier
        appVersionInfo.Version = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
        
        ApiUtils.checkVersionUpdate(data: appVersionInfo, onSuccess: { (resp) in
            guard resp.NeedUpgrade ?? false else {
                return
            }
            
            let updateCheckAlert = UIAlertController(title: "App Update Notice", message: "New version \(resp.Version!) is available", preferredStyle: .alert)
            var positActionStyle:UIAlertAction.Style
            if(resp.AlwaysUpgrade!) {
                positActionStyle = .cancel
            } else {
                positActionStyle = .default
                let negatAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    updateCheckAlert.dismiss(animated: true, completion: nil)
                }
                updateCheckAlert.addAction(negatAction)
            }
            let positAction = UIAlertAction(title: "Check", style: positActionStyle, handler: { (action) in
                UIApplication.shared.open(URL(string: "itms-apps://itunes.apple.com/app/id581548701")!, options:
                    [:], completionHandler: nil)
                if(!resp.AlwaysUpgrade!) {
                    updateCheckAlert.dismiss(animated: true, completion: nil)
                }
            })
            updateCheckAlert.addAction(positAction)
            vc.present(updateCheckAlert, animated: true, completion: nil)
            print("")
        }) { (error) in
            print("\(error.description)")
        }
    }
}
